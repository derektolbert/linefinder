# Developed by Aptus Engineering, Inc. <https://aptus.aero>
# See LICENSE.md file in project root directory

import cv2
import numpy as np
from collections import Counter

STRAIGHT_THRESH = 0.95
MIN_LINE_LENGTH = 5
MAX_LINE_GAP = 10

def findDirections(img):
    minLineLength = 5
    maxLineGap = 15

    lines = cv2.HoughLinesP(img, 0.5, np.pi/1800, 10, minLineLength, maxLineGap)

    directions = []
    for line in lines:
        x1,y1,x2,y2 = line[0]

        p1 = np.array([y1,x1])
        p2 = np.array([y2, x2])

        dv = abs( [p1 - p2] / np.linalg.norm(p1 - p2) )
        dv = (dv[0][0], dv[0][1])

        directions.append(dv)

    # Convert to a list of unique wall directions sorted by frequency
    directions = list(dict(Counter(directions)).keys())

    # Inititalize with most frequently occuring wall direction
    confirmed_directions = [directions[0]]

    # Find all unique directions
    for new_direction in directions:

        dots = []
        for direction in confirmed_directions:
            dots.append(np.dot(direction, new_direction))

        if all(dot < STRAIGHT_THRESH for dot in dots):
            confirmed_directions.append(new_direction)

    return confirmed_directions

def checkNextStep(img, pt, reference_pt=None, dv=[0,1]):
    # sequence of possible moves 
    operations = [[-1,-1], [-1, 0], [-1, 1], [0, 1], [1, 1], [1, 0], [1, -1], [0, -1]] 
    y, x = pt
    
    # Find step directions to check for wall (adjacent pixels to pt + dv)
    idx = operations.index(dv)
    steps = [operations[idx - 1], operations[idx], operations[idx - 7]] 

    # Remove the inverse of dv to avoid checking our previous line
    operations.remove([dv[0] * -1, dv[1] * -1])

    # Check for corners
    for step in operations:
        if img[y + step[0], x + step[1]] != 0 and step not in steps:
            return False, ()

    for step in steps:
        
        if img[y + step[0], x + step[1]] == 1:
            # If we have a reference, compute dot product and compare
            if reference_pt is not None:
                # Compare <pt - reference_pt> to <dv>
                ref = pt - reference_pt

                # Convert to unit length
                ref = ref / np.linalg.norm(ref)

                dot = abs(np.dot(ref, dv))

                if dot > STRAIGHT_THRESH:
                    return True, (y + step[0], x + step[1])
                else:
                    break
            else:
                return True, (y + step[0], x + step[1])

    # Check for line breaks within MAX_LINE_GAP
    end_step = np.multiply(dv, MAX_LINE_GAP - 2) // 2
    end_steps = [(step + end_step) * 2 for step in steps]
    pt = np.add(list(pt), dv)
    end_pts = np.array([pt + step for step in end_steps] + [list(pt)])

    # Compute bounding rectangle
    y_min, y_max = np.min(end_pts[:,0]), np.max(end_pts[:,0])
    x_min, x_max = np.min(end_pts[:,1]), np.max(end_pts[:,1])

    # Check if any true in rectangle
    if any(img[y_min:y_max, x_min:x_max].flatten() == 1):        
        next_y, next_x = np.where(img[y_min:y_max, x_min:x_max])

        return True, (y + dv[0], x + dv[1])

    return False, ()

def lineFinder(img):
    # Find directions of walls in image
    directions = findDirections(img)

    print(f'Found {len(directions)} directions in image')

    # Storage
    total_lines = []

    # Binarize image
    img = img.astype('int8')
    img2 = img.copy()

    for dv in directions:
        print(f'Checking lines in {dv} direction')

        # Convert to a list of ints
        dv = [np.round(val).astype('int8') for val in dv]
        
        # Get nonzero coordinates
        pts = np.rollaxis(np.array(np.where(img2 == 1)),0,2)
        
        # Storage for all lines in this direction
        lines = []

        # Loop until we find all lines in that direction
        while len(pts) > 0: 

            # Initialize vars
            i = 0
            refPt = None
            idx = np.argmin(pts[:,0])
            pt = pts[idx]
            line = []

            # Follow along pt looking for a line
            while True:
                if i > 10:
                    refPt = line[0]

                okay, next_pt = checkNextStep(img2, pt, refPt, dv)

                if okay:
                    img2[pt[0], pt[1]] = -1
                    line.append(pt)
                    pt = next_pt

                else:
                    img2[pt[0], pt[1]] = -1
                    line.append(pt)

                    # Save long lines, constrain to given direction
                    if len(line) > MIN_LINE_LENGTH:

                        # Project end of line in the direction of dv
                        mag = np.dot(line[-1] - line[0], dv)
                        end = line[0] + np.multiply(dv, mag)

                        lines.append([line[0], end])

                    break

                i += 1
            
            pts = np.rollaxis(np.array(np.where(img2 == 1)),0,2)
        
        total_lines.append(lines)
        img2 = img.copy()

    return total_lines