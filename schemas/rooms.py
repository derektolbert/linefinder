# Developed by Aptus Engineering, Inc. <https://aptus.aero>
# See LICENSE.md file in project root directory

from dataclasses import dataclass
from typing import List

types = ['hallway', 'lobby', 'general/bedroom', 'bathroom/closet', 'storage/elevator']

@dataclass
class Room:
    _id: str
    contour: List
    roomType: str
    sqft: float
    perimeter: float
    n_doors: int
    n_windows: int
    connected: List[str] # IDs to connected rooms

