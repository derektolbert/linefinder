# Developed by Aptus Engineering, Inc. <https://aptus.aero>
# See LICENSE.md file in project root directory

import cv2
import numpy as np
import matplotlib.pyplot as plt
from collections import Counter
from skimage.morphology import skeletonize

from lineFinder import lineFinder
import random

img = 'data/Microtel 5 Color.png'

img = cv2.imread(img)
img = cv2.resize(img, (1800, 1200))

# Extract features from image
walls = np.all(img == [255, 0, 0], axis=-1).astype('uint8')
windows = np.all(img == [0, 255, 0], axis=-1).astype('uint8')
doors = np.all(img == [0, 0, 255], axis=-1).astype('uint8')
segmented = (walls + windows + doors).astype('uint8')

skel = skeletonize(segmented)

total_lines = lineFinder(skel.astype('uint8'))

line_img = np.zeros(img.shape)
for direction in total_lines:
    for i,line in enumerate(direction):
        start = np.flip(line[0])
        end = np.flip(line[-1])

        color = [[255,0,0], [0,255,0],[0,0,255],[255, 255, 0],
                               [0,255,255], [255,0,255]][i%6]
        
        line_img = cv2.line(line_img, tuple(start), tuple(end), 
                    color, 2)

plt.subplot(121)
plt.imshow(img)
plt.title('Original')

plt.subplot(122)
plt.imshow(line_img)
plt.title('Walls')

plt.show()