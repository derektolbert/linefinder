# Developed by Aptus Engineering, Inc. <https://aptus.aero>
# See LICENSE.md file in project root directory

import cv2
import numpy as np
from uuid import uuid1
import matplotlib.pyplot as plt

MIN_THRESHOLD = 200
SIZE = (24.0, 36.0)
KERNEL = np.ones((10,10),np.uint8)
IN_TO_FT = 1 /8

#TODO: Add codes.json

def processRoom(room, floorData):
    image = floorData['image']
    windows = floorData['windows']
    doors = floorData['doors']
    walls = floorData['walls']

    # Create room ID, get area and perimeter
    _id = str(uuid1())
    sqft = cv2.contourArea(room) * floorData['px_conversion']
    perim = cv2.arcLength(room, True) * floorData['px_conversion']

    # Create a binary mask for this room
    blank = np.zeros(image.shape[:2])
    mask = cv2.drawContours(blank, [room], 0, 1, -1).astype('bool')    

    # Find the windows in this room
    win_diff = mask * windows
    ws = cv2.findContours(win_diff, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[0]
    n_windows = len(ws)

    # Find the doors in this room
    door_diff = mask * doors
    ds = cv2.findContours(door_diff, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[0]
    n_doors = len(ds)

    # Find the center of room
    ys, xs = mask.nonzero()

    center = (int(np.mean(ys)), int(np.mean(xs)))

    if n_doors > 0 or n_windows > 0:
        
        roomData = {
            '_id': str(uuid1()),
            'mask': mask,
            'center': center,
            'area': sqft,
            'perimeter': perim,
            'n_doors': n_doors,
            'n_windows': n_windows
        }

        return roomData

    else:
        return {}

def sortRooms(floorData):
    """
    Floors = a set of rooms
        all the same props as rooms with the addition of the following:
            1. List of all rooms contained in floor
            2. Wall, window, door dimensions are contained in the floors object
    """

    # Sort by area
    rooms = floorData['rooms']
    rooms = sorted(rooms, key=lambda x: x['area'], reverse=True)

    #Sort into seperate floors (i.e. > 1 floor per image)
    floors = []
    
    for room in rooms:

        # Append largest room as a floor
        if len(floors) == 0:
            floors.append({
                'floor': 1,
                'rooms': [],
                'mask': room['mask'],
                'area': room['area'],
                'perimeter': room['perimeter'],
                'n_doors': room['n_doors'],
                'n_windows': room['n_windows'],
                'wall_dims': [],
                'window_dims': []
            })

        # If new rooms are outside of previous floors, they must be new floors
        appended = False
        for floor in floors:
            if floor['mask'][room['center']]: # Check if room is in floor
                floor['rooms'].append(room)
                appended = True
                break

        if not appended:
            floors.append({
                'floor': len(floors) + 1,
                'rooms': [],
                'mask': room['mask'],
                'area': room['area'],
                'perimeter': room['perimeter'],
                'n_doors': room['n_doors'],
                'n_windows': room['n_windows'],
                'wall_dims': [],
                'window_dims': []
            })

    # Remove individual rooms as these are no longer needed
    del floorData['rooms']

    floorData['floors'] = floors

    return floorData    
        

def processImage(image):
    # Initialize data storage
    floorData = {
        "image": image
    }

    floorData['px_conversion'] = SIZE[0] / image.shape[0] * (IN_TO_FT)

    # Extract features from image
    floorData["walls"] = np.all(image == [255, 0, 0], axis=-1).astype('uint8')
    floorData["windows"] = np.all(image == [0, 255, 0], axis=-1).astype('uint8')
    floorData["doors"] = np.all(image == [0, 0, 255], axis=-1).astype('uint8')
    floorData['segmented'] = ( floorData['walls'] + floorData['windows'] + 
                               floorData['doors'] )

    # Dilate features
    for feature in ['walls', 'windows', 'doors', 'segmented']:
        floorData[feature] = cv2.dilate(floorData[feature], KERNEL, 1)
 
    # Get contours of all rooms on floor
    rooms, hier = cv2.findContours(floorData['segmented'], cv2.RETR_TREE, 
                                  cv2.CHAIN_APPROX_SIMPLE) 

    # Storage for individual room data
    floorData['rooms'] = []

    # Process all rooms
    for room in rooms:
        roomData = processRoom(room, floorData)

        if roomData:
            floorData['rooms'].append(roomData)

    floors = sortRooms(floorData)

    return floors

